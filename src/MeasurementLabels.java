public enum MeasurementLabels {

    // This is the list of accepted labels and the number of measured values
    BIAS("bias", 6),
    DISTANCE("distance", 2),
    FLEXIBILITY("flexibility", 2),
    FREQUENCY("frequency", 4),
    HUMIDITY("humidity", 3),
    SPEED("speed", 5);

    String labelString;
    final int NUMBER_OF_DATA_PARTS;

    // For a type B measurement
    double minValue = Double.POSITIVE_INFINITY, maxValue = Double.NEGATIVE_INFINITY;
    boolean measurementDone = false;

    MeasurementLabels(String labelString, int numberOfDataParts) {
        this.labelString = labelString;
        this.NUMBER_OF_DATA_PARTS = numberOfDataParts;
    }

    String getLabelString() { return this.labelString; }

    int getNumberOfDataParts() { return this.NUMBER_OF_DATA_PARTS; }

    public void setMinValue(Double value) {
        this.minValue = value;
    }

    public void setMaxValue(Double value) {
        this.maxValue = value;
    }

    public double getMinValue() {
        return this.minValue;
    }

    public double getMaxValue() {
        return this.maxValue;
    }

    public boolean isMeasurementDone() {
        return this.measurementDone;
    }

    public void setMeasurementDone() {
        this.measurementDone = true;
    }
}
