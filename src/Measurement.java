import java.util.Arrays;
import java.util.StringTokenizer;

public class Measurement {

    // Each measurement has a vector of measured data, its own label, a type, and a final result
    private double data[];
    private MeasurementLabels label = null;
    private boolean typeB;
    private double finalResult;

    public Measurement(String measurementData, boolean typeB) {

        if(measurementData.isEmpty()) {
            System.err.println("Couldn't create an instance of Measurement: the data string is empty!");
        }

        // By using StringTokenizer I am able to extract all data from a string. The first data is a label
        StringTokenizer st = new StringTokenizer(measurementData);
        String dataLabel = st.nextElement().toString();

        // Then we have to find this label in the list of accepted labels
        for(MeasurementLabels Label : MeasurementLabels.values()) {
            if(Label.getLabelString().equals(dataLabel)) {
                this.label = Label;
                break;
            }
        }

        // If we don't find any accepted label, show an error message
        if(label == null) {
            System.err.println("Couldn't resolve your measurement label. Allowed labels are " +
                    Arrays.toString(MeasurementLabels.values()));
        }

        // Fill the data array with the measured values
        int elementsCounter = 0;
        this.data = new double[label.getNumberOfDataParts()];
        while(st.hasMoreElements() && elementsCounter < label.getNumberOfDataParts()) {
            this.data[elementsCounter++] = Double.parseDouble(st.nextElement().toString());
        }

        // Determine the measurement type
        this.typeB = typeB;

    }

    public double getFinalResult() {
        return finalResult;
    }

    public MeasurementLabels getLabel() {
        return label;
    }

    @Override
    public String toString() {
        return "Measurement{" +
                "data=" + Arrays.toString(data) +
                ", label=" + label +
                '}';
    }

    public void countResult() {
        // Each type of the measurement has its own formula to calculate the final value
        switch(label) {
            case BIAS:
                this.finalResult = data[0] * data[1] + data[2] - data[3] - data[4] + data[5];
                break;

            case DISTANCE:
                if(data[1] == 0) {
                    //System.err.println("Cannot divide by zero!");
                    this.finalResult = Double.POSITIVE_INFINITY;
                } else {
                    this.finalResult = data[0] / data[1];
                }
                break;

            case FLEXIBILITY:
                this.finalResult =  data[0] / 2 * data[1];
                break;

            case FREQUENCY:
                if(data[3] == 0) {
                    //System.err.println("Cannot divide by zero!");
                    this.finalResult = Double.POSITIVE_INFINITY;
                } else {
                    this.finalResult = (data[0] + data[1] + data[2]) / data[3];
                }
                break;

            case HUMIDITY:
                this.finalResult = data[0] * data[1] * data[2];
                break;

            case SPEED:
                if(data[4] == 0) {
                    //System.err.println("Cannot divide by zero!");
                    this.finalResult = Double.POSITIVE_INFINITY;
                } else {
                    this.finalResult = (data[0] + data[1]) * (data[2] + data[3]) / data[4];
                }
                break;

            default:
                System.err.println("We couldn't resolve the type of your label.");
                break;
        }

        // If it is a type B measurement, we have to additionally find min and max values
        if(typeB) determineMinAndMaxValues(this.finalResult);

        // Mark this type of measurement as done (for determining what measurements to show afterwards)
        this.label.setMeasurementDone();
    }

    // Set min and max values to each enum reference
    private void determineMinAndMaxValues(double value) {
        if(value < this.label.getMinValue()) {
            this.label.setMinValue(value);
        }
        if(value > this.label.getMaxValue()) {
            this.label.setMaxValue(value);
        }
    }
}
