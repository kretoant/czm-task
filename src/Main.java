import java.io.*;
import java.util.*;

public class Main {

    public static void main(String args[]) throws IOException{

        FileReader fileInput = null;
        FileWriter fileOutput = null;

        // If we haven't got the arguments we want
        if(args.length != 2) {
            System.err.println("You haven't specified the input and output files!");
            return;
        }

        fileInput = new FileReader(args[0]);
        fileOutput = new FileWriter(args[1]);

        BufferedReader inputReader = new BufferedReader(fileInput);

        //Since we have two types of measurements, we have to determine it at the beginning of the input
        boolean typeB = inputReader.readLine().equals("B");
        inputReader.readLine();

        //We will store each measurement as a reference of Measurement class in an ArrayList for more convenient iteration and comparison
        ArrayList<Measurement> MeasurementList = new ArrayList<>();

        String inputLine;
        while((inputLine = inputReader.readLine()) != null) {
            MeasurementList.add(new Measurement(inputLine, typeB));
        }

        fileInput.close();

        for(Measurement m : MeasurementList) {
            m.countResult();
        }

        PrintWriter pw = new PrintWriter(fileOutput);

        //After we're done with counting the final data, we have to determine, what we want to output
        if(typeB) {
            //For this type of measurement, we have to take every measurement type that was in our input measurements
            //and take its max and min value, then sort them according with their labels' alphabetical order
            List<MeasurementLabelValue> MinMaxMeasurementsList = new ArrayList<>();

            for(MeasurementLabels Label : MeasurementLabels.values()) {
                //If this type of measurement has been done, add it
                if(Label.isMeasurementDone()) {
                    MinMaxMeasurementsList.add(new MeasurementLabelValue(Label.getLabelString(), Label.getMinValue()));
                    MinMaxMeasurementsList.add(new MeasurementLabelValue(Label.getLabelString(), Label.getMaxValue()));
                }
            }

            //Sort this list using custom comparator
            Collections.sort(MinMaxMeasurementsList);

            //Output to the file
            for(MeasurementLabelValue entry : MinMaxMeasurementsList) {
                pw.println(entry.label + " " + entry.value);
            }
        } else {
            //In this type we just need to print the result of each measurement
            for(Measurement m : MeasurementList) {
                pw.println(m.getLabel().getLabelString() + " " + m.getFinalResult());
            }
        }

        fileOutput.close();

    }
}

//Utility class to store two values in an ArrayList and for the following comparison
class MeasurementLabelValue implements Comparable<MeasurementLabelValue> {
    String label;
    double value;

    public MeasurementLabelValue(String label, double value) {
        this.label = label;
        this.value = value;
    }


    @Override
    public int compareTo(MeasurementLabelValue o) {
        return (this.label).compareTo(o.label);
    }
}
